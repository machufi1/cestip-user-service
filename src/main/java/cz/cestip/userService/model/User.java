package cz.cestip.userService.model;

import javax.persistence.*;

@Entity
@Table(name = "CESTIP_USER")
@NamedQuery(name = "User.findByEmail", query = "SELECT e FROM User e WHERE e.email = :email")
public class User extends AbstractEntity{

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
