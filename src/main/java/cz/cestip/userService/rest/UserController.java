package cz.cestip.userService.rest;

import cz.cestip.userService.dao.UserDao;
import cz.cestip.userService.exception.NotFoundException;
import cz.cestip.userService.model.User;
import cz.cestip.userService.rest.util.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
    
    @Autowired
    UserDao userDao;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> registerUser(@RequestBody User user) {
        userDao.persist(user);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{email}", user.getEmail());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getByMail(@PathVariable String email) {
        final User employee = userDao.findByEmail(email);
        if (employee == null) {
            throw NotFoundException.create("User", email);
        }
        return employee;
    }
}
