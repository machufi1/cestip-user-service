package cz.cestip.userService.exception;

public class PersistenceException extends CestipException {

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }
}
