package cz.cestip.userService.exception;

/**
 * Base for all application-specific exceptions.
 */
public class CestipException extends RuntimeException {

    public CestipException() {
    }

    public CestipException(String message) {
        super(message);
    }

    public CestipException(String message, Throwable cause) {
        super(message, cause);
    }

    public CestipException(Throwable cause) {
        super(cause);
    }
}
